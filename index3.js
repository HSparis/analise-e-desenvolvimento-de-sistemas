const express = require('express');
const bodyParser = require('body-parser');
const app = express();
app.use(bodyParser.json);
app.use(bodyParser.urlencoded({extended:true}));
const port = 8000;

//habilitar servidor
app.listen(port,()=>{
    console.log("Projeto executando na porta: " + port );
});

//recurso de consulta

app.get('/cliente',(req, res)=>{
    console.log("Acessando o recurso Cliente");
    res.send("{message:/get cliente}"); 
});

app.get('/pesquisa', (req, res)=>{
    let dados = req.query;
    res.send(dados.nome + " " + dados.sobrenome);
});

app.get('/pesquisa/cliente/:codigo', (req, res)=>{
    let id = req.params.codigo;
    res.send("Dados do cliente: " + id);
});

app.post('/cliente/gravar', (req, res)=>{
    let valores = req.body;
    console.log("Nome: " + valores.nome);
    console.log("Sobrenome: " + valores.sobrenome);
    console.log("Idade: " + valores.idade);
    res.send('Sucesso');
});